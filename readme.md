# magnet + plotter

![](_img/out.gif)

## Getting Started    

You will need the magnetmike v1, the software [inotify](https://en.wikipedia.org/wiki/Inotify), inkscape cli and [pstoedit](http://www.pstoedit.net/) a software that transform eps to any format. This is simply a daemon that send to a printer any new drawing saved to a folder.      

You might need to creat a php file to save the generated drawing to the write folder.    

This bash script is also use in the [hp2drawing]() tools.

NB : the plotter / printer don't need any driver as it is isntalled "raw" via cups and use with the CLI as followed :     

```bash
lp -d mydevice -o raw "myfile"
```


## Author

[Bonjour Monde](http://bonjourmonde.net)

## License

[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)
