#!/bin/bash
DIR="/home/pi/Downloads"

inotifywait -m -e create --format %f "${DIR}" | while read NEWFILE
do
	EXT="${NEWFILE##*.}"
	if [ "$EXT" == "svg" ]
	then
	input="$DIR/$NEWFILE"
	output="$DIR/${NEWFILE//[[:space:]]}"
	mv "$input" "$output"
	##mv $input $output
	inkscape "${output}" -P "$DIR/output.ps"
	echo "file to postscript ok"
	pstoedit -f plot-hpgl "$DIR/output.ps" "$DIR/output.hpgl"
	echo "file to hpgl ok"
	lp -d sketchmate -o raw "$DIR/output.hpgl"
	fi
done
